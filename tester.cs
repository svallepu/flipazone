﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flipazone
{
    internal class tester//declaring a tester class
    {
        static void Main(String[] args)
        {
            /** calling a customer class by passing parameters like int id,string name
             * string email-id,string phone number,string district*/
            Customer customer = new Customer(816, "sravani", "sravanivallepu2601", "6305692972", "warangal");
            Console.WriteLine(customer);
            Console.WriteLine("-----------------------------------------------------------");

            //calling a order class by passing parameters like order id order status and price
             Order newOrder = new Order(2435, "Shipped", 2000);
            Console.WriteLine(newOrder.ToString());
            Console.WriteLine("------------------------------------------------------------");

            /**calling a product class by passing parameters like product name 
             * product description,price of producr**/
            Product product = new Product(100, "watch", "Titan", 2000);
            Console.WriteLine(product);
            Console.WriteLine("------------------------------------------------------------");
            /** calling a rating class to give rating for the product by passing 
             * parameters like customer id,rating,review**/
            Rating rating = new Rating(customer.customer_id, 4, "dress quality is good");
            Console.Write(rating.ToString());
            Console.WriteLine("-------------------------------------------------------------");                
             /** calling address class by passing parameters like string house number
             * string village,string narasampet,string pincode,string district,string state,string country**/
   
            Address address = new Address("1-79", "khanapuram", "narasampet", "506132","warangal","telangana","india");
            Console.WriteLine(address);
        }
    }

}

