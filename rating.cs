﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flipazone//declaring a name space

{
    /// <summary>
    /// This is the implimentation of class rating,in which we get rating for the product
    /// </summary>
    internal class Rating//declaring a internal class 
    {
        
        private int id;
        public int rating;
        public string review;

        /// <summary>
        /// created a constructor with arguments calling rating  class instance variables 
        /// </summary>
        /// <param name="customer_id"></param>
        /// <param name="rating"></param>
        /// <param name="review"></param>
        public Rating(int customer_id, int rating, string review)
        {
            this.id = customer_id;
            this.rating = rating;
            this.review = review;

            if (!(rating == 1 || rating == 2 || rating == 3 || rating == 4 || rating == 5))
            {
                Console.WriteLine(" please give valid rating");

            }
            else if (review.Length < 3)
            {
                Console.WriteLine("Please provide valid review");
            }


        }

        //This is a override of tostring method to print the Rating object in human readable form 
        override
        public String ToString()
        //an object to its string representation so that it is suitable for display
        {

            String stringToReturn = "This is the rating details of a product: \n";
            stringToReturn += "customer_Id          : " + this.id + "\n";
            stringToReturn += "rating               : " + this.rating + "\n";
            stringToReturn += "review               : " + this.review + "\n";

            return stringToReturn;

        }
    }
} 
    

