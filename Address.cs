﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flipazone
{
    /// <summary>
    /// This is the implentation of class address ,providing address details.
    /// </summary>
    internal class Address
    { 
        public string housenumber;
        public string street;
        public string city;
        public string pincode;
        public string district;
        public string state;
        public string country;
       /// <summary>
       /// This is the constructor with an arguments calling the instance variables
       /// </summary>
       /// <param name="housenumber"></param>
       /// <param name="street"></param>
       /// <param name="city"></param>
       /// <param name="pincode"></param>
       /// <param name="district"></param>
       /// <param name="state"></param>
       /// <param name="country"></param>
        public Address(string housenumber, string street, string city, string pincode, string district, string state, string country)
        {
            this.housenumber = housenumber;
            this.street = street;
            this.city = city;
            this.pincode = pincode;
            this.district = district;
            this.state = state;
            this.country = country;
        }

        
        override
        //An object to its string representation so that it is suitable for display
        public String ToString()
        {
            String stringToReturn = "Customer address details: \n";
            stringToReturn += "housenumber               : " + this.housenumber + "\n";
            stringToReturn += "street                    : " + this.street + "\n";
            stringToReturn += "city                      : " + this.city + "\n";
            stringToReturn += "pincode                   : " + this.pincode + "\n";
            stringToReturn += "district                  : " + this.district + "\n";
            stringToReturn += "state                     : " + this.state + "\n";
            stringToReturn += " country                  : " + this.country + "\n";


            return stringToReturn;

        }
    }
}






        

    


