﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flipazone
{
    /**
     * This is an implementation of a class name Customer and declared some instance variables
     */
    internal class Customer//declaring a class for customer
    {
        public int customer_id;
        private string name;
        private string email;
        private string phonenumber;
        private string address;
        //declaring a constructor for customer class
        /// <summary>
        /// This is the constructor with an arguments calling the instance variables
        /// </summary>
        /// <param name="customer_id"></param>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="phonenumber"></param>
        /// <param name="address"></param>
        public Customer(int customer_id, string name, string email, string phonenumber, string address)
        {
            this.customer_id = customer_id;
            this.name = name;
            this.email = email;
            this.phonenumber = phonenumber;
            this.address = address;
        }

        //here converting Customer object to string representation
        override
        public String ToString()
        {
            String stringToReturn = "This is a Customer object details: \n";
            stringToReturn += "Id            : " + this.customer_id         + "\n";
            stringToReturn += "Name          : " + this.name                + "\n";
            stringToReturn += "E-mail        : " + this.email               + "\n";
            stringToReturn += "Phone Number  : " + this.phonenumber         + "\n";
            stringToReturn += "Address       : " + this.address             + "\n";
            return stringToReturn;
        }
    }
}

    


