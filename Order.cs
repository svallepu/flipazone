﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flipazone
{
    /// <summary>
    /// this is the implimentation of class order ,in which we get details of the order
    /// </summary>
    internal class Order//declaring a class for order
    {
            public int Id;
            public string orderstatus;
            //public string Description;
            public float Price;
        /// <summary>
        /// created a constructor with arguments calling order  class instance variables 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="orderstatus"></param>
        /// <param name="price"></param>
        public Order(int Id, string orderstatus, int price)
            {
                this.Id = Id;
                this.orderstatus = orderstatus;
                this.Price = price;
            }



            override
            //an object to its string representation so that it is suitable for display
            public String ToString()
            {

                String stringToReturn = "This is a product object details: \n";
                stringToReturn += "name                 : " + this.Id + "\n";
                stringToReturn += "description          : " + this.orderstatus + "\n";
                stringToReturn += "price                : " + this.Price + "\n";

                return stringToReturn;

            }
        }
    }





