﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flipazone
{
    /// <summary>
    /// This is the implementation of class Product ,in which we get details of the product.
    /// </summary>
    internal class Product
    {
        //Initialising instance variables
        public int id;
        public string Name;
        public string Description;
        public int Price;

        /// <summary>
        /// created a constructor with arguments calling product class instance variables 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Name"></param>
        /// <param name="Description"></param>
        /// <param name="Price"></param>
        public Product(int id, String Name, string Description, int Price)
        {
            //Console.WriteLine("Default constructor called.");
            this.id = id;
            this.Name = Name;
            this.Description = Description;
            this.Price = Price;

            // verify input data, if input is bad throw exception
            try
            {

                if (Name.Trim().Length < 2)
                {
                    throw new IOException(" Name is too short ");
                }
                else if (Description.Trim().Length < 2)
                {
                    throw new IOException("Description is too short");
                }
                else if (Price < 2)
                {
                    throw new Exception("Price is too low or negative");
                }
                else
                {
                    //intializing instance things


                    //=this.Id = ++Id_counter+ "";

                    this.Name = Name;
                    this.Description = Description;
                    this.Price = Price;

                }
            }
            catch
            {
                throw;
            }


        }

        public int Id_counter { get; private set; }

        //This is a override of tostring method to print the product object in human readable form 

        override
            //an object to its string representation so that it is suitable for display
            public String ToString()
        {
            String stringToReturn = "This is a product object details: \n";
            stringToReturn += "name                : " + this.Name + "\n";
            stringToReturn += "description         : " + this.Description + "\n";
            stringToReturn += "price               : " + this.Price + "\n";

            return stringToReturn;
        }
    }
}



            

     
    

    


